@echo This will delete savefile.lev, qgrass.spec dist/ and build/
@pause

rmdir dist /s /q
rmdir build /s /q
del savefile.lev
del qgrass.spec

pause