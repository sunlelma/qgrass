import tkinter as tk
import tkinter.filedialog as filedialog
import ctypes
import sys


class Application(tk.Frame):
    def __init__(self, master):
        super().__init__(master)
        self.pack()
        self.create_widgets()

    def create_widgets(self):

        self.button1 = tk.Button(
            self, text="Choose level file", command=self.SelectImagePopup)
        self.button1.pack(side="top")
        self.label1 = tk.Entry(self, width=80)
        self.label1.insert(0, "Image File")
        self.label1.pack(side="top")

        self.button2 = tk.Button(
            self, text="Choose save file (do not overwrite initial file)",
            command=self.SelectSavePopup)
        self.button2.pack(side="top")
        self.label2 = tk.Entry(self, width=80)
        self.label2.insert(0, "savefile.lev")
        self.label2.pack(side="top")

        self.button100 = tk.Button(
            self, text="CREATE QGRASS LEVEL!", command=self.CreateLevel)
        self.button100.pack(side="top")

    def SelectImagePopup(self):
        imgfile = filedialog.askopenfilename(
            filetypes=[(".lev files", "*.lev"), ("All", "*")])
        self.label1.delete(0, len(self.label1.get()))
        self.label1.insert(0, imgfile)

    def SelectSavePopup(self):
        savename = filedialog.asksaveasfilename(
            filetypes=[("Elastomania Level File", "*.lev"), ("All", "*")])
        if(savename[-4:] != ".lev"):
            savename = savename+".lev"
        self.label2.delete(0, len(self.label2.get()))
        self.label2.insert(0, savename)

    def CreateLevel(self):
        openf = self.label1.get()
        savef = self.label2.get()
        try:
            data = open(openf, 'rb')
            data = bytearray(data.read())
        except:
            ctypes.windll.user32.MessageBoxW(
                0, "Error: Unable to open level file! Verify that the path is"
                "correct and the file is valid.\r\n\r\n"+str(sys.exc_info()),
                "Error!", 0)
            return
        data[94:102] = [int.from_bytes(
            b'default\0'[k:k+1], byteorder='little', signed=False)
            for k in range(8)]
        for i in range(len(data)-6):
            if(data[i:i+6] == bytearray(b'QGRASX')):
                data[i+5] = int.from_bytes(b'S',
                                           byteorder='little', signed=False)
        try:
            with open(savef, 'wb') as f:
                f.write(data)
            ctypes.windll.user32.MessageBoxW(0, "Success!", "Yay!", 0)
        except:
            ctypes.windll.user32.MessageBoxW(
                0, "Error: Unable to save level file! Verify that the file is"
                "not being used and that the path is valid.\r\n" +
                str(sys.exc_info()), "Error!", 0)
        return


root = tk.Tk()
root.title("QGRASS.exe")
app = Application(master=root)

app.mainloop()
