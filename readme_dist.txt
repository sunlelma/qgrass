QGRASS.EXE 1.01, by sunl

1. Make a level in any editor using the lgr "dgrassx.lgr" (set lgr to dgrassx.lgr)
2. Use the QGRASX texture to put grass texture
3. Run QGRASS.EXE
4. QGRASS.EXE will change the lgr to "default" and will change the grass texture form "QGRASX" to "QGRASS"
5. Grass texture now exists in a normal level with default.lgr

qgrass_ori.lev is a level made using the above instructions (1-2)
After running QGRASS.EXE, qgrass_exe.lev is the produced file.



----


"QGRASX" is case-sensitive