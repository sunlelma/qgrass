@echo This will delete savefile.lev, qgrass.spec, dist/ and build/ then generate the distributable program in dist/
@pause
rmdir dist /s /q
rmdir build /s /q
del savefile.lev
del qgrass.spec

pyinstaller --onefile qgrass.py

copy /y readme_dist.txt dist\readme.txt
copy /y qgra_ori.lev dist\qgra_ori.lev
copy /y qgra_exe.lev dist\qgra_exe.lev
copy /y dgrassx.lgr dist\dgrassx.lgr

@pause